﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.ExceptionClasses
{
    class ListNullException: Exception
    {
        public ListNullException(): base("Lista eshte bosh")
        {

        }

        public ListNullException(string message): base(message)
        {

        }
    }
}
