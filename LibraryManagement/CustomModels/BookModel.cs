﻿using LibraryManagement.Interface;
using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.CustomModels
{
    class BookModel
    {
        public int ID { get; set; }
        public int AccesionNo { get; set; }
        public string ISBN { get; set; }
        public string Titulli { get; set; }
        public int KategoriaID { get; set; }
        public int Sasia { get; set; }
        public bool IsAvailable { get; set; }
    }
}
