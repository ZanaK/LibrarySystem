﻿using LibraryManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.CustomModels
{
    class UserModel
    {
        public int ID { get; set; }
        public string Emri { get; set; }
        public string Mbiemri { get; set; }
        public System.DateTime Datelindja { get; set; }
        public string Gjinia { get; set; }
        public string Adresa { get; set; }
        public string NumriTelefonit { get; set; }
        public string VitiStudimeve { get; set; }
        public string Email { get; set; }
    }
}
