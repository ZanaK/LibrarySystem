﻿using LibraryManagement.Classes;
using LibraryManagement.CustomModels;
using LibraryManagement.Enums;
using LibraryManagement.ExceptionClasses;
using LibraryManagement.Interface;
using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagement
{
    public partial class ManageBooks : Form
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly DbContext context = new LibraryDBEntities();
        List<Librat> lstLibrat = new List<Librat>();

        public ManageBooks()
        {
            InitializeComponent();
            //krijojme servisin per menaxhimin e librave
            this.unitOfWork = new UnitOfWork();
        }

        //merr te gjithe librat dhe mbush datagridin te lista e librave dhe editimi i tyre
        private void GetBooks()
        {
            try
            {
                var books = this.unitOfWork.BooksRepository.GetAll().Where(c=>c.IsDeleted != true).ToList(); //per mos me bo dy her call ne databaze
                if (books == null || books.Count == 0)
                {
                    throw new ListNullException();
                }
                dataGridView1.DataSource = books.Select(c => new { c.AccesionNo, c.ISBN, c.Titulli }).ToList();
                dataGridView2.DataSource = books.Select(c => new { c.AccesionNo, c.ISBN, c.Titulli }).ToList();
                lstLibrat.Clear();
                lstLibrat.AddRange(books);
            }
            catch (ListNullException ex)
            {
                MessageBox.Show(ex.Message, "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ClearControls()
        {
            txtAccessionNo.Text = string.Empty;
            txtBookTitle.Text = string.Empty;
            txtISBN.Text = string.Empty;
            txtAuthor.Text = string.Empty;
            CbCategory.Text = string.Empty;
            CbCategory.SelectedIndex = -1;

            txtAccessionNo_Upd.Text = string.Empty;
            txtBookTitle_Upd.Text = string.Empty;
            txtISBN_Upd.Text = string.Empty;
            txtAuthor_Upd.Text = string.Empty;
            cbCategory_Upd.Text = string.Empty;
            cbCategory_Upd.SelectedIndex = -1;
        }

        private bool ValidateControls()
        {
            bool IsValid = false;

            if (txtAccessionNo.Text == "" || txtAccessionNo.Text == null || (txtAccessionNo.Text.ToUpper() != txtAccessionNo.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem numer tek AccessionNo", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtBookTitle.Text == "" || txtBookTitle.Text == null || (txtBookTitle.Text.ToUpper() == txtBookTitle.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek bookTitle", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtISBN.Text == "" || txtISBN.Text == null)
            {
                MessageBox.Show("Ploteso ISBN", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtAuthor.Text == "" || txtAuthor.Text == null || (txtAuthor.Text.ToUpper() == txtAuthor.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek autori", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (CbCategory.Text == "" || CbCategory.Text == null || CbCategory.SelectedIndex == -1)
            {
                MessageBox.Show("Zgjedh ose jep kategorine", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                IsValid = true;
            }
            return IsValid;
        }

        private void ManageBooks_Load(object sender, EventArgs e)
        {
            GetBooks();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void btnAddBooks_Click(object sender, EventArgs e)
        {
            //validohen kontrollat
            if (ValidateControls())
            {
                //krijojhet objekti per librin qe shtohet
                Librat objLibrat = new Librat()
                {
                    AccesionNo = Convert.ToInt32(txtAccessionNo.Text),
                    ISBN = txtISBN.Text,
                    Titulli = txtBookTitle.Text,
                    KategoriaID = EnumConverteer.CategoryNameToCategoryNumber(CbCategory.Text)
                };

                //duke perdoru servisin, studenti shtohet ne databaze 
                this.unitOfWork.BooksRepository.Add(objLibrat);

                //krijojhet objekti per autoret e librave qe shtohen qe shtohet
                LibriAutoret objAutoret = new LibriAutoret()
                {
                    LibriID = this.unitOfWork.BooksRepository.GetAll().Where(c=>c.IsDeleted != true).OrderByDescending(c => c.ID).FirstOrDefault().ID,
                    Autori = txtAuthor.Text
                };

                //duke perdoru servisin, autori shtohet ne databaze 
                this.unitOfWork.BookAuthorsRepository.Add(objAutoret);
                ClearControls();
                //dataGridView1.DataSource = this.unitOfWork.BooksRepository.GetAll().ToList().Select(c => new { c.AccesionNo, c.ISBN, c.Titulli}).ToList();
                //dataGridView2.DataSource = this.unitOfWork.BooksRepository.GetAll().ToList().Select(c => new { c.AccesionNo, c.ISBN, c.Titulli}).ToList();
                GetBooks();
            }

        }

        private void txtKerko_TextChanged(object sender, EventArgs e)
        {
            //ne textchange te textboxit edhe bon search ne databaze per studenta ne baze te emrit
            if (txtKerko.Text != null && (txtKerko.Text.ToUpper() != txtKerko.Text.ToLower()))
                FilterByName(txtKerko.Text);
            else
                dataGridView2.DataSource = this.unitOfWork.BooksRepository.GetAll().Where(c=> c.IsDeleted != true).Select(c => new { c.AccesionNo, c.ISBN, c.Titulli }).ToList();
        }

        private void FilterByName(string titull)
        {
            dataGridView2.DataSource = this.unitOfWork.BooksRepository.GetAll().Where(c => c.Titulli.StartsWith(titull) && c.IsDeleted != true).Select(c => new { c.AccesionNo, c.ISBN, c.Titulli}).ToList();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var accessionNo = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            Librat objUserModel = this.unitOfWork.BooksRepository.GetAll().Where(c => c.AccesionNo == accessionNo && c.IsDeleted != true).FirstOrDefault();

            txtAccessionNo_Upd.Text = objUserModel.AccesionNo.ToString();
            txtAuthor_Upd.Text = objUserModel.LibriAutorets.FirstOrDefault().Autori;
            txtBookTitle_Upd.Text = objUserModel.Titulli;
            txtISBN_Upd.Text = objUserModel.ISBN;
            cbCategory_Upd.Text = EnumConverteer.CategoryNumberToCategoryName(objUserModel.KategoriaID).ToString();
        }

        private void btnUpdateDGV_Click(object sender, EventArgs e)
        {
            var accessionNo = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            Librat objUserModel = this.unitOfWork.BooksRepository.GetAll().Where(c => c.AccesionNo == accessionNo && c.IsDeleted != true).FirstOrDefault();

            objUserModel.AccesionNo = Convert.ToInt32(txtAccessionNo_Upd.Text);
            objUserModel.ISBN = txtISBN_Upd.Text;
            objUserModel.Titulli = txtBookTitle_Upd.Text;
            objUserModel.KategoriaID = EnumConverteer.CategoryNameToCategoryNumber(cbCategory_Upd.Text);

            this.unitOfWork.BooksRepository.Update(objUserModel);
            GetBooks();
            ClearControls();
        }

        private void btnDeleteBook_Click(object sender, EventArgs e)
        {
            var accessionNo = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            Librat objToDelete = this.unitOfWork.BooksRepository.GetAll().Where(c => c.AccesionNo == accessionNo && c.IsDeleted != true).FirstOrDefault();
            objToDelete.IsDeleted = true;
            this.unitOfWork.BooksRepository.Update(objToDelete);
            GetBooks();
            ClearControls();
        }
    }
}
