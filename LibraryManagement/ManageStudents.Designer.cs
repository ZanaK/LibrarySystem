﻿namespace LibraryManagement
{
    partial class ManageStudents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageStudents));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabAddStudent = new System.Windows.Forms.TabPage();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtContactNumber = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAddStudents = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.radMale = new System.Windows.Forms.RadioButton();
            this.radFemale = new System.Windows.Forms.RadioButton();
            this.tabEditStudent = new System.Windows.Forms.TabPage();
            this.dgvStudents = new System.Windows.Forms.DataGridView();
            this.txtEmaail = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.txtYeaar = new System.Windows.Forms.TextBox();
            this.txtAddres = new System.Windows.Forms.TextBox();
            this.txtLast = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.radM = new System.Windows.Forms.RadioButton();
            this.radF = new System.Windows.Forms.RadioButton();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.lblKerko = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.dgvStudentet = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.tabControl1.SuspendLayout();
            this.tabAddStudent.SuspendLayout();
            this.tabEditStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).BeginInit();
            this.tabSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentet)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabAddStudent);
            this.tabControl1.Controls.Add(this.tabEditStudent);
            this.tabControl1.Controls.Add(this.tabSearch);
            this.tabControl1.Location = new System.Drawing.Point(44, 90);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(601, 375);
            this.tabControl1.TabIndex = 0;
            // 
            // tabAddStudent
            // 
            this.tabAddStudent.BackColor = System.Drawing.Color.LightGray;
            this.tabAddStudent.Controls.Add(this.dateTimePicker1);
            this.tabAddStudent.Controls.Add(this.txtEmail);
            this.tabAddStudent.Controls.Add(this.txtContactNumber);
            this.tabAddStudent.Controls.Add(this.txtYear);
            this.tabAddStudent.Controls.Add(this.txtAddress);
            this.tabAddStudent.Controls.Add(this.txtLastName);
            this.tabAddStudent.Controls.Add(this.txtFirstName);
            this.tabAddStudent.Controls.Add(this.Label28);
            this.tabAddStudent.Controls.Add(this.btnClear);
            this.tabAddStudent.Controls.Add(this.btnAddStudents);
            this.tabAddStudent.Controls.Add(this.Label8);
            this.tabAddStudent.Controls.Add(this.Label2);
            this.tabAddStudent.Controls.Add(this.Label3);
            this.tabAddStudent.Controls.Add(this.Label7);
            this.tabAddStudent.Controls.Add(this.Label4);
            this.tabAddStudent.Controls.Add(this.Label5);
            this.tabAddStudent.Controls.Add(this.Label6);
            this.tabAddStudent.Controls.Add(this.radMale);
            this.tabAddStudent.Controls.Add(this.radFemale);
            this.tabAddStudent.Location = new System.Drawing.Point(4, 22);
            this.tabAddStudent.Name = "tabAddStudent";
            this.tabAddStudent.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddStudent.Size = new System.Drawing.Size(593, 349);
            this.tabAddStudent.TabIndex = 0;
            this.tabAddStudent.Text = "Add New Student";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(124, 90);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker1.TabIndex = 65;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(373, 119);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(129, 20);
            this.txtEmail.TabIndex = 57;
            // 
            // txtContactNumber
            // 
            this.txtContactNumber.Location = new System.Drawing.Point(373, 63);
            this.txtContactNumber.Name = "txtContactNumber";
            this.txtContactNumber.Size = new System.Drawing.Size(129, 20);
            this.txtContactNumber.TabIndex = 55;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(373, 89);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(129, 20);
            this.txtYear.TabIndex = 56;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(373, 37);
            this.txtAddress.MaxLength = 50;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(129, 20);
            this.txtAddress.TabIndex = 54;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(124, 63);
            this.txtLastName.MaxLength = 50;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(129, 20);
            this.txtLastName.TabIndex = 48;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(124, 37);
            this.txtFirstName.MaxLength = 50;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(129, 20);
            this.txtFirstName.TabIndex = 46;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(335, 126);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(32, 13);
            this.Label28.TabIndex = 64;
            this.Label28.Text = "Email";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(318, 205);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 32);
            this.btnClear.TabIndex = 63;
            this.btnClear.Text = "Cl&ear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAddStudents
            // 
            this.btnAddStudents.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddStudents.Location = new System.Drawing.Point(193, 205);
            this.btnAddStudents.Name = "btnAddStudents";
            this.btnAddStudents.Size = new System.Drawing.Size(88, 32);
            this.btnAddStudents.TabIndex = 62;
            this.btnAddStudents.Text = "&Add";
            this.btnAddStudents.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddStudents.UseVisualStyleBackColor = true;
            this.btnAddStudents.Click += new System.EventHandler(this.btnAddStudents_Click);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(283, 70);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(84, 13);
            this.Label8.TabIndex = 61;
            this.Label8.Text = "Contact Number";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(64, 43);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(54, 13);
            this.Label2.TabIndex = 47;
            this.Label2.Text = "FirstName";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(63, 68);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(55, 13);
            this.Label3.TabIndex = 49;
            this.Label3.Text = "LastName";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(338, 96);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(29, 13);
            this.Label7.TabIndex = 60;
            this.Label7.Text = "Year";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(76, 129);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(42, 13);
            this.Label4.TabIndex = 51;
            this.Label4.Text = "Gender";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(50, 96);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(68, 13);
            this.Label5.TabIndex = 53;
            this.Label5.Text = "Date Of Birth";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(322, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(45, 13);
            this.Label6.TabIndex = 59;
            this.Label6.Text = "Address";
            // 
            // radMale
            // 
            this.radMale.AutoSize = true;
            this.radMale.Location = new System.Drawing.Point(195, 125);
            this.radMale.Name = "radMale";
            this.radMale.Size = new System.Drawing.Size(48, 17);
            this.radMale.TabIndex = 58;
            this.radMale.TabStop = true;
            this.radMale.Text = "Male";
            this.radMale.UseVisualStyleBackColor = true;
            // 
            // radFemale
            // 
            this.radFemale.AutoSize = true;
            this.radFemale.Location = new System.Drawing.Point(133, 125);
            this.radFemale.Name = "radFemale";
            this.radFemale.Size = new System.Drawing.Size(59, 17);
            this.radFemale.TabIndex = 52;
            this.radFemale.TabStop = true;
            this.radFemale.Text = "Female";
            this.radFemale.UseVisualStyleBackColor = true;
            // 
            // tabEditStudent
            // 
            this.tabEditStudent.BackColor = System.Drawing.Color.LightGray;
            this.tabEditStudent.Controls.Add(this.dtpDate);
            this.tabEditStudent.Controls.Add(this.dgvStudents);
            this.tabEditStudent.Controls.Add(this.txtEmaail);
            this.tabEditStudent.Controls.Add(this.txtID);
            this.tabEditStudent.Controls.Add(this.txtNumber);
            this.tabEditStudent.Controls.Add(this.txtYeaar);
            this.tabEditStudent.Controls.Add(this.txtAddres);
            this.tabEditStudent.Controls.Add(this.txtLast);
            this.tabEditStudent.Controls.Add(this.txtName);
            this.tabEditStudent.Controls.Add(this.label9);
            this.tabEditStudent.Controls.Add(this.btnReset);
            this.tabEditStudent.Controls.Add(this.btnUpdate);
            this.tabEditStudent.Controls.Add(this.label10);
            this.tabEditStudent.Controls.Add(this.label11);
            this.tabEditStudent.Controls.Add(this.label12);
            this.tabEditStudent.Controls.Add(this.label13);
            this.tabEditStudent.Controls.Add(this.label14);
            this.tabEditStudent.Controls.Add(this.label15);
            this.tabEditStudent.Controls.Add(this.label16);
            this.tabEditStudent.Controls.Add(this.label17);
            this.tabEditStudent.Controls.Add(this.radM);
            this.tabEditStudent.Controls.Add(this.radF);
            this.tabEditStudent.Location = new System.Drawing.Point(4, 22);
            this.tabEditStudent.Name = "tabEditStudent";
            this.tabEditStudent.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditStudent.Size = new System.Drawing.Size(593, 349);
            this.tabEditStudent.TabIndex = 1;
            this.tabEditStudent.Text = "Edit Student";
            // 
            // dgvStudents
            // 
            this.dgvStudents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudents.Location = new System.Drawing.Point(6, 194);
            this.dgvStudents.Name = "dgvStudents";
            this.dgvStudents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudents.Size = new System.Drawing.Size(578, 150);
            this.dgvStudents.TabIndex = 65;
            this.dgvStudents.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStudents_CellContentClick);
            // 
            // txtEmaail
            // 
            this.txtEmaail.Location = new System.Drawing.Point(380, 108);
            this.txtEmaail.MaxLength = 50;
            this.txtEmaail.Name = "txtEmaail";
            this.txtEmaail.Size = new System.Drawing.Size(129, 20);
            this.txtEmaail.TabIndex = 57;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(129, 26);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(129, 20);
            this.txtID.TabIndex = 44;
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(380, 52);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(129, 20);
            this.txtNumber.TabIndex = 55;
            // 
            // txtYeaar
            // 
            this.txtYeaar.Location = new System.Drawing.Point(380, 78);
            this.txtYeaar.Name = "txtYeaar";
            this.txtYeaar.Size = new System.Drawing.Size(129, 20);
            this.txtYeaar.TabIndex = 56;
            // 
            // txtAddres
            // 
            this.txtAddres.Location = new System.Drawing.Point(380, 26);
            this.txtAddres.MaxLength = 50;
            this.txtAddres.Name = "txtAddres";
            this.txtAddres.Size = new System.Drawing.Size(129, 20);
            this.txtAddres.TabIndex = 54;
            // 
            // txtLast
            // 
            this.txtLast.Location = new System.Drawing.Point(129, 78);
            this.txtLast.MaxLength = 50;
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(129, 20);
            this.txtLast.TabIndex = 48;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(129, 52);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(129, 20);
            this.txtName.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 64;
            this.label9.Text = "Email";
            // 
            // btnReset
            // 
            this.btnReset.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnReset.Location = new System.Drawing.Point(439, 153);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 32);
            this.btnReset.TabIndex = 63;
            this.btnReset.Text = "Cl&ear";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdate.Location = new System.Drawing.Point(314, 153);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(88, 32);
            this.btnUpdate.TabIndex = 62;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Student ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(290, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "Contact Number";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(69, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "FirstName";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(68, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "LastName";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(345, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 60;
            this.label14.Text = "Year";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(81, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "Gender";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(55, 111);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 53;
            this.label16.Text = "Date Of Birth";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(329, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 13);
            this.label17.TabIndex = 59;
            this.label17.Text = "Address";
            // 
            // radM
            // 
            this.radM.AutoSize = true;
            this.radM.Location = new System.Drawing.Point(200, 140);
            this.radM.Name = "radM";
            this.radM.Size = new System.Drawing.Size(48, 17);
            this.radM.TabIndex = 58;
            this.radM.TabStop = true;
            this.radM.Text = "Male";
            this.radM.UseVisualStyleBackColor = true;
            // 
            // radF
            // 
            this.radF.AutoSize = true;
            this.radF.Location = new System.Drawing.Point(138, 140);
            this.radF.Name = "radF";
            this.radF.Size = new System.Drawing.Size(59, 17);
            this.radF.TabIndex = 52;
            this.radF.TabStop = true;
            this.radF.Text = "Female";
            this.radF.UseVisualStyleBackColor = true;
            // 
            // tabSearch
            // 
            this.tabSearch.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabSearch.Controls.Add(this.lblKerko);
            this.tabSearch.Controls.Add(this.textBox9);
            this.tabSearch.Controls.Add(this.dgvStudentet);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(593, 349);
            this.tabSearch.TabIndex = 2;
            this.tabSearch.Text = "List of Students";
            // 
            // lblKerko
            // 
            this.lblKerko.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKerko.Location = new System.Drawing.Point(10, 42);
            this.lblKerko.Name = "lblKerko";
            this.lblKerko.Size = new System.Drawing.Size(84, 20);
            this.lblKerko.TabIndex = 2;
            this.lblKerko.Text = "Search :";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(100, 42);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 1;
            this.textBox9.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
            // 
            // dgvStudentet
            // 
            this.dgvStudentet.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStudentet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudentet.Location = new System.Drawing.Point(10, 99);
            this.dgvStudentet.Name = "dgvStudentet";
            this.dgvStudentet.Size = new System.Drawing.Size(578, 234);
            this.dgvStudentet.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(44, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(158, 22);
            this.label18.TabIndex = 3;
            this.label18.Text = "Manage Students";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(129, 105);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(129, 20);
            this.dtpDate.TabIndex = 66;
            // 
            // ManageStudents
            // 
            this.AcceptButton = this.btnUpdate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.CancelButton = this.btnReset;
            this.ClientSize = new System.Drawing.Size(735, 468);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "ManageStudents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManageStudents";
            this.Load += new System.EventHandler(this.ManageStudents_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabAddStudent.ResumeLayout(false);
            this.tabAddStudent.PerformLayout();
            this.tabEditStudent.ResumeLayout(false);
            this.tabEditStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).EndInit();
            this.tabSearch.ResumeLayout(false);
            this.tabSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAddStudent;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.TextBox txtContactNumber;
        internal System.Windows.Forms.TextBox txtYear;
        internal System.Windows.Forms.TextBox txtAddress;
        internal System.Windows.Forms.TextBox txtLastName;
        internal System.Windows.Forms.TextBox txtFirstName;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Button btnClear;
        internal System.Windows.Forms.Button btnAddStudents;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.RadioButton radMale;
        internal System.Windows.Forms.RadioButton radFemale;
        private System.Windows.Forms.TabPage tabEditStudent;
        private System.Windows.Forms.DataGridView dgvStudents;
        internal System.Windows.Forms.TextBox txtEmaail;
        internal System.Windows.Forms.TextBox txtID;
        internal System.Windows.Forms.TextBox txtNumber;
        internal System.Windows.Forms.TextBox txtYeaar;
        internal System.Windows.Forms.TextBox txtAddres;
        internal System.Windows.Forms.TextBox txtLast;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Button btnReset;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.RadioButton radM;
        internal System.Windows.Forms.RadioButton radF;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.Label lblKerko;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.DataGridView dgvStudentet;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }
}