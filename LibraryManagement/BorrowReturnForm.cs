﻿using LibraryManagement.Interface;
using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagement
{
    public partial class BorrowReturnForm : Form
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly DbContext context = new LibraryDBEntities();

        public BorrowReturnForm()
        {
            InitializeComponent();
            this.unitOfWork = new UnitOfWork();
        }

        private bool ValidateControls()
        {
            bool IsValid = false;

            if (txtStudentBorrow.Text == "" || txtStudentBorrow.Text == null || (txtStudentBorrow.Text.ToUpper() != txtStudentBorrow.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem numer tek ID", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(txtISBNBorrow.Text == "" || txtISBNBorrow.Text == null || (txtISBNBorrow.Text.ToUpper() == txtISBNBorrow.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek ISBN", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else 
            {
                IsValid = true;
            }

            return IsValid;
        }

        private void ClearControls()
        {
            txtStudentBorrow.Text = string.Empty;
            txtISBNBorrow.Text = string.Empty;
            txtLate.Text = string.Empty;
            dtpDate.Value = DateTime.Now.Date;
        }

        private void btnBorrow_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                LibratHuazuara objLibratHuazuara = new LibratHuazuara()
                {
                    StudentID = Convert.ToInt32(txtStudentBorrow.Text),
                    LibriID = this.unitOfWork.BooksRepository.GetAll().Where(c => c.ISBN == txtISBNBorrow.Text).FirstOrDefault().ID,
                    DataHuazimit = DateTime.Now.Date,
                    Vonesa = "null",
                    DataKthimit = DateTime.Now.Date

                };

                this.unitOfWork.BorrowedBooksRepository.Add(objLibratHuazuara);
                ClearControls();
                GetBorrowedBooks();
            }
        }

        private void BorrowReturnForm_Load(object sender, EventArgs e)
        {
            lblBorrowDate.Text = DateTime.Now.Date.ToShortDateString();
            GetBorrowedBooks();
        }

        private void GetBorrowedBooks()
        {
            dataGridView1.DataSource = this.unitOfWork.BorrowedBooksRepository.GetAll().Select(c => new { c.ID, c.Librat.Titulli, c.Studentet.Emri, c.DataHuazimit }).ToList();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            var ID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            var model = this.unitOfWork.BorrowedBooksRepository.GetById(ID);
            model.Vonesa = txtLate.Text;
            model.DataKthimit = dtpDate.Value.Date;

            this.unitOfWork.BorrowedBooksRepository.Update(model);
            GetBorrowedBooks();
            ClearControls();
        }


    }
}
