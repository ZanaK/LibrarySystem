﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagement
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnManageStudents_Click(object sender, EventArgs e)
        {
            //therrasim formen per menaxhimin e studenteve
            ManageStudents frmManageStudents = new ManageStudents();
            frmManageStudents.ShowDialog();
        }

        private void btnManageBooks_Click(object sender, EventArgs e)
        {
            ManageBooks frmManageBooks = new ManageBooks();
            frmManageBooks.ShowDialog();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnBorrowBook_Click(object sender, EventArgs e)
        {
            BorrowReturnForm frmBorrowReturn = new BorrowReturnForm();
            frmBorrowReturn.ShowDialog();
        }
    }
}
