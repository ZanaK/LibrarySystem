﻿using LibraryManagement.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Classes
{
    public static class EnumConverteer
    {
        public static int CategoryNameToCategoryNumber(string categoryName)
        {
            int categoryID = 0;
            List<string> lstEnumElements = new List<string>();

            foreach (Categories p in Enum.GetValues(typeof(Categories)))
            {
                lstEnumElements.Add(p.ToString());
            }

            if (lstEnumElements[0] == categoryName)
                categoryID = (int)Categories.Mathematics;
            else if (lstEnumElements[1] == categoryName)
                categoryID = (int)Categories.Computer;
            else if (lstEnumElements[2] == categoryName)
                categoryID = (int)Categories.Science;
            else if (lstEnumElements[3] == categoryName)
                categoryID = (int)Categories.Programming;
            else if (lstEnumElements[4] == categoryName)
                categoryID = (int)Categories.Novel;
            else if (lstEnumElements[5] == categoryName)
                categoryID = (int)Categories.Religion;

            return categoryID;
        }

        public static string CategoryNumberToCategoryName(int categoryNumber)
        {
            string categoryID = null;
            List<int> lstEnumElements = new List<int>();

            foreach (Categories p in Enum.GetValues(typeof(Categories)))
            {
                lstEnumElements.Add(Convert.ToInt32(p));
            }

            if (lstEnumElements[0] == categoryNumber)
                categoryID = Categories.Mathematics.ToString();
            else if (lstEnumElements[1] == categoryNumber)
                categoryID = Categories.Computer.ToString();
            else if (lstEnumElements[2] == categoryNumber)
                categoryID = Categories.Science.ToString();
            else if (lstEnumElements[3] == categoryNumber)
                categoryID = Categories.Programming.ToString();
            else if (lstEnumElements[4] == categoryNumber)
                categoryID = Categories.Novel.ToString();
            else if (lstEnumElements[5] == categoryNumber)
                categoryID = Categories.Religion.ToString();

            return categoryID;
        }
    }
}
