﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagement.ExceptionClasses;
using LibraryManagement.Interface;
using LibraryManagement.Model;

namespace LibraryManagement
{
    public partial class ManageStudents : Form
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly DbContext context = new LibraryDBEntities();
        List<Studentet> lstStudentet = new List<Studentet>();

        //konstruktor
        public ManageStudents()
        {
            InitializeComponent();
            //krijojme servisin per menaxhimin e studenteve
            this.unitOfWork = new UnitOfWork();
        }

        private void btnAddStudents_Click(object sender, EventArgs e)
        {
            //validohen kontrollat
            if (ValidateControls())
            {
                //krijojhet objekti per studentin qe shtohet
                Studentet objStudentet = new Studentet()
                {
                    Emri = txtFirstName.Text,
                    Mbiemri = txtLastName.Text,
                    Gjinia = radMale.Checked ? "M" : "F",
                    VitiStudimeve = txtYear.Text,
                    Adresa = txtAddress.Text,
                    Email = txtEmail.Text,
                    NumriTelefonit = txtContactNumber.Text,
                    Datelindja = Convert.ToDateTime(dateTimePicker1.Value.Date)
                };

                //duke perdoru servisin, studenti shtohet ne databaze 
                this.unitOfWork.StudentsRepository.Add(objStudentet);
                ClearControls();
                GetStudents();
            }
           

        }

        private bool ValidateControls()
        {
            bool IsValid = false;

            if (txtFirstName.Text == "" || txtFirstName.Text == null || (txtFirstName.Text.ToUpper() == txtFirstName.Text.ToLower() ))
            {
                MessageBox.Show("Jep vetem shkronja tek emri", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtLastName.Text == "" || txtLastName.Text == null || (txtLastName.Text.ToUpper() == txtLastName.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek mbiemri", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(dateTimePicker1.Value == null)
            {
                MessageBox.Show("Jep daten tek ditelindja", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(radFemale.Checked == false && radMale.Checked == false)
            {
                MessageBox.Show("Cakto gjinine", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(txtAddress.Text == "" || txtAddress.Text == null || (txtAddress.Text.ToUpper() == txtAddress.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek adresa", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtContactNumber.Text == "" || txtContactNumber.Text == null || (txtContactNumber.Text.ToUpper() != txtContactNumber.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem numer tek telefoni", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtYear.Text == "" || txtYear.Text == null || (txtYear.Text.ToUpper() != txtYear.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem numer tek viti", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtEmail.Text == "" || txtEmail.Text == null || (txtEmail.Text.ToUpper() == txtEmail.Text.ToLower()))
            {
                MessageBox.Show("Jep vetem shkronja tek emaili", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                IsValid = true;
            }
            return IsValid;
        }

        private void ClearControls()
        {
            txtAddress.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtYear.Text = string.Empty;
            txtContactNumber.Text = string.Empty;
            radFemale.Checked = false;
            radMale.Checked = false;
            dateTimePicker1.Value = DateTime.Now.Date;

            txtAddres.Text = string.Empty;
            txtEmaail.Text = string.Empty;
            txtID.Text = string.Empty;
            txtLast.Text = string.Empty;
            txtNumber.Text = string.Empty;
            txtYeaar.Text = string.Empty;
            txtName.Text = string.Empty;
            dtpDate.Value = DateTime.Now.Date;
        }

        //merr te gjithe studentet dhe mbush datagridin te lista e studentave dhe editimi i tyre
        private void GetStudents()
        {
            try
            {
                var students = this.unitOfWork.StudentsRepository.GetAll(); //per mos me bo dy her call ne databaze
                if (students == null || students.Count == 0)
                {
                    throw new ListNullException();
                }
                dgvStudentet.DataSource = students.Select(c => new {c.Emri, c.Mbiemri, c.Gjinia , Viti = c.VitiStudimeve, c.Email}).ToList();
                dgvStudents.DataSource = students.Select(c => new { c.Emri, c.Mbiemri, c.Gjinia, Viti = c.VitiStudimeve, c.Email }).ToList();
                lstStudentet.Clear();
                lstStudentet.AddRange(students);
            }
            catch (ListNullException ex)
            {
                MessageBox.Show(ex.Message, "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void ManageStudents_Load(object sender, EventArgs e)
        {
            GetStudents(); //ne load te formes
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            //ne textchange te textboxit edhe bon search ne databaze per studenta ne baze te emrit
            if (textBox9.Text != null && (textBox9.Text.ToUpper() != textBox9.Text.ToLower()))
                FilterByName(textBox9.Text);
            else
                dgvStudentet.DataSource = this.unitOfWork.StudentsRepository.GetAll().Where(c => c.IsDeleted != true).Select(c => new { c.Emri, c.Mbiemri, c.Gjinia, Viti = c.VitiStudimeve, c.Email }).ToList();
        }

        private void FilterByName(string name)
        {
            dgvStudentet.DataSource = this.unitOfWork.StudentsRepository.GetAll().Where(c => c.Emri.StartsWith(name) && c.IsDeleted != true).Select(c => new { c.Emri, c.Mbiemri, c.Gjinia, Viti = c.VitiStudimeve, c.Email }).ToList();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var name = dgvStudents.SelectedRows[0].Cells[0].Value.ToString();
            var surname = dgvStudents.SelectedRows[0].Cells[1].Value.ToString();
            Studentet objUserModel = this.unitOfWork.StudentsRepository.GetAll().Where(c => c.Emri == name && c.Mbiemri == surname && c.IsDeleted != true).FirstOrDefault();

            objUserModel.Emri = txtName.Text;
            objUserModel.Mbiemri = txtLast.Text;
            objUserModel.Adresa = txtAddres.Text;
            objUserModel.NumriTelefonit = txtNumber.Text;
            objUserModel.Datelindja = dtpDate.Value.Date;
            objUserModel.VitiStudimeve = txtYeaar.Text;
            objUserModel.Email = txtEmaail.Text;
            objUserModel.Gjinia = radM.Checked == true ? "M" : "F";

            this.unitOfWork.StudentsRepository.Update(objUserModel);
            GetStudents();
            ClearControls();
        }

        private void dgvStudents_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var name = dgvStudents.SelectedRows[0].Cells[0].Value.ToString();
            var surname = dgvStudents.SelectedRows[0].Cells[1].Value.ToString();
            Studentet objUserModel = this.unitOfWork.StudentsRepository.GetAll().Where(c => c.Emri == name && c.Mbiemri == surname && c.IsDeleted != true).FirstOrDefault();

            txtAddres.Text = objUserModel.Adresa.ToString();
            txtEmaail.Text = objUserModel.Email;
            txtID.Text = objUserModel.ID.ToString();
            txtLast.Text = objUserModel.Mbiemri;
            txtNumber.Text = objUserModel.NumriTelefonit;
            txtYeaar.Text = objUserModel.VitiStudimeve;
            txtName.Text = objUserModel.Emri;
            dtpDate.Value = Convert.ToDateTime(objUserModel.Datelindja);

            if (objUserModel.Gjinia == "M")
                radM.Checked = true;
            else
                radF.Checked = true;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
    }
}
