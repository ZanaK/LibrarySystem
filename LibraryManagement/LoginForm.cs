﻿using LibraryManagement.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagement
{
    public partial class LoginForm : Form
    {
        private readonly IUnitOfWork unitOfWork;

        public LoginForm()
        {
            InitializeComponent();
            this.unitOfWork = new UnitOfWork();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var user = this.unitOfWork.StudentsRepository.GetAll().Where(c => c.Email == txtUsername.Text && c.Password == txtPassword.Text).FirstOrDefault();

            if (user != null)
            {
                ClearControls();
                MainForm frmMain = new MainForm();
                frmMain.ShowDialog();
            }
            else
            {
                MessageBox.Show("Username ose Passwordi nuk eshte valid", "Lajmerim", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
        }

        void ClearControls()
        {
            txtPassword.Text = string.Empty;
            txtUsername.Text = string.Empty;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
    }
}
