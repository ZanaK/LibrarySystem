﻿namespace LibraryManagement
{
    partial class ManageBooks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageBooks));
            this.tabAddBooks = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnClear = new System.Windows.Forms.Button();
            this.CbCategory = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.btnAddBooks = new System.Windows.Forms.Button();
            this.txtAccessionNo = new System.Windows.Forms.TextBox();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.txtBookTitle = new System.Windows.Forms.TextBox();
            this.txtISBN = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnDeleteBook = new System.Windows.Forms.Button();
            this.btnUpdateDGV = new System.Windows.Forms.Button();
            this.cbCategory_Upd = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtAccessionNo_Upd = new System.Windows.Forms.TextBox();
            this.txtAuthor_Upd = new System.Windows.Forms.TextBox();
            this.txtBookTitle_Upd = new System.Windows.Forms.TextBox();
            this.txtISBN_Upd = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtKerko = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.tabAddBooks.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabAddBooks
            // 
            this.tabAddBooks.Controls.Add(this.tabPage1);
            this.tabAddBooks.Controls.Add(this.tabPage2);
            this.tabAddBooks.Controls.Add(this.tabPage3);
            this.tabAddBooks.Location = new System.Drawing.Point(32, 76);
            this.tabAddBooks.Name = "tabAddBooks";
            this.tabAddBooks.SelectedIndex = 0;
            this.tabAddBooks.Size = new System.Drawing.Size(594, 332);
            this.tabAddBooks.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.btnClear);
            this.tabPage1.Controls.Add(this.CbCategory);
            this.tabPage1.Controls.Add(this.Label4);
            this.tabPage1.Controls.Add(this.btnAddBooks);
            this.tabPage1.Controls.Add(this.txtAccessionNo);
            this.tabPage1.Controls.Add(this.txtAuthor);
            this.tabPage1.Controls.Add(this.txtBookTitle);
            this.tabPage1.Controls.Add(this.txtISBN);
            this.tabPage1.Controls.Add(this.Label1);
            this.tabPage1.Controls.Add(this.Label2);
            this.tabPage1.Controls.Add(this.Label3);
            this.tabPage1.Controls.Add(this.Label5);
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(586, 306);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Add new Books";
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(313, 191);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(86, 31);
            this.btnClear.TabIndex = 36;
            this.btnClear.Text = "Clear";
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // CbCategory
            // 
            this.CbCategory.FormattingEnabled = true;
            this.CbCategory.Items.AddRange(new object[] {
            "Mathematics",
            "Computer\tScience",
            "Programming",
            "Science",
            "Novel",
            "Religion"});
            this.CbCategory.Location = new System.Drawing.Point(251, 140);
            this.CbCategory.Name = "CbCategory";
            this.CbCategory.Size = new System.Drawing.Size(134, 21);
            this.CbCategory.TabIndex = 35;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(189, 143);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(49, 13);
            this.Label4.TabIndex = 34;
            this.Label4.Text = "Category";
            // 
            // btnAddBooks
            // 
            this.btnAddBooks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddBooks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBooks.Location = new System.Drawing.Point(207, 191);
            this.btnAddBooks.Name = "btnAddBooks";
            this.btnAddBooks.Size = new System.Drawing.Size(86, 31);
            this.btnAddBooks.TabIndex = 33;
            this.btnAddBooks.Text = "Add";
            this.btnAddBooks.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddBooks.UseVisualStyleBackColor = true;
            this.btnAddBooks.Click += new System.EventHandler(this.btnAddBooks_Click);
            // 
            // txtAccessionNo
            // 
            this.txtAccessionNo.Location = new System.Drawing.Point(251, 36);
            this.txtAccessionNo.Name = "txtAccessionNo";
            this.txtAccessionNo.Size = new System.Drawing.Size(134, 20);
            this.txtAccessionNo.TabIndex = 25;
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(251, 114);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(134, 20);
            this.txtAuthor.TabIndex = 31;
            // 
            // txtBookTitle
            // 
            this.txtBookTitle.Location = new System.Drawing.Point(251, 88);
            this.txtBookTitle.Name = "txtBookTitle";
            this.txtBookTitle.Size = new System.Drawing.Size(134, 20);
            this.txtBookTitle.TabIndex = 29;
            // 
            // txtISBN
            // 
            this.txtISBN.Location = new System.Drawing.Point(251, 62);
            this.txtISBN.Name = "txtISBN";
            this.txtISBN.Size = new System.Drawing.Size(134, 20);
            this.txtISBN.TabIndex = 27;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(165, 43);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(73, 13);
            this.Label1.TabIndex = 26;
            this.Label1.Text = "Accession No";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(206, 68);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(32, 13);
            this.Label2.TabIndex = 28;
            this.Label2.Text = "ISBN";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(183, 93);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(55, 13);
            this.Label3.TabIndex = 30;
            this.Label3.Text = "Book Title";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(200, 114);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(38, 13);
            this.Label5.TabIndex = 32;
            this.Label5.Text = "Author";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.btnDeleteBook);
            this.tabPage2.Controls.Add(this.btnUpdateDGV);
            this.tabPage2.Controls.Add(this.cbCategory_Upd);
            this.tabPage2.Controls.Add(this.Label6);
            this.tabPage2.Controls.Add(this.txtAccessionNo_Upd);
            this.tabPage2.Controls.Add(this.txtAuthor_Upd);
            this.tabPage2.Controls.Add(this.txtBookTitle_Upd);
            this.tabPage2.Controls.Add(this.txtISBN_Upd);
            this.tabPage2.Controls.Add(this.Label7);
            this.tabPage2.Controls.Add(this.Label8);
            this.tabPage2.Controls.Add(this.Label9);
            this.tabPage2.Controls.Add(this.Label11);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(586, 306);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Edit Books";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 151);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(566, 150);
            this.dataGridView1.TabIndex = 63;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnDeleteBook
            // 
            this.btnDeleteBook.Location = new System.Drawing.Point(316, 61);
            this.btnDeleteBook.Name = "btnDeleteBook";
            this.btnDeleteBook.Size = new System.Drawing.Size(75, 30);
            this.btnDeleteBook.TabIndex = 62;
            this.btnDeleteBook.Text = "Delete";
            this.btnDeleteBook.UseVisualStyleBackColor = true;
            this.btnDeleteBook.Click += new System.EventHandler(this.btnDeleteBook_Click);
            // 
            // btnUpdateDGV
            // 
            this.btnUpdateDGV.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnUpdateDGV.Location = new System.Drawing.Point(235, 62);
            this.btnUpdateDGV.Name = "btnUpdateDGV";
            this.btnUpdateDGV.Size = new System.Drawing.Size(75, 29);
            this.btnUpdateDGV.TabIndex = 61;
            this.btnUpdateDGV.Text = "Update";
            this.btnUpdateDGV.UseVisualStyleBackColor = false;
            this.btnUpdateDGV.Click += new System.EventHandler(this.btnUpdateDGV_Click);
            // 
            // cbCategory_Upd
            // 
            this.cbCategory_Upd.FormattingEnabled = true;
            this.cbCategory_Upd.Items.AddRange(new object[] {
            "Mathematics",
            "Computer\tScience",
            "Programming",
            "Science",
            "Novel",
            "Religion"});
            this.cbCategory_Upd.Location = new System.Drawing.Point(85, 115);
            this.cbCategory_Upd.Name = "cbCategory_Upd";
            this.cbCategory_Upd.Size = new System.Drawing.Size(114, 21);
            this.cbCategory_Upd.TabIndex = 60;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(30, 118);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(49, 13);
            this.Label6.TabIndex = 59;
            this.Label6.Text = "Category";
            // 
            // txtAccessionNo_Upd
            // 
            this.txtAccessionNo_Upd.Location = new System.Drawing.Point(85, 11);
            this.txtAccessionNo_Upd.Name = "txtAccessionNo_Upd";
            this.txtAccessionNo_Upd.Size = new System.Drawing.Size(114, 20);
            this.txtAccessionNo_Upd.TabIndex = 51;
            // 
            // txtAuthor_Upd
            // 
            this.txtAuthor_Upd.Location = new System.Drawing.Point(85, 89);
            this.txtAuthor_Upd.Name = "txtAuthor_Upd";
            this.txtAuthor_Upd.Size = new System.Drawing.Size(114, 20);
            this.txtAuthor_Upd.TabIndex = 57;
            // 
            // txtBookTitle_Upd
            // 
            this.txtBookTitle_Upd.Location = new System.Drawing.Point(85, 63);
            this.txtBookTitle_Upd.Name = "txtBookTitle_Upd";
            this.txtBookTitle_Upd.Size = new System.Drawing.Size(114, 20);
            this.txtBookTitle_Upd.TabIndex = 56;
            // 
            // txtISBN_Upd
            // 
            this.txtISBN_Upd.Location = new System.Drawing.Point(85, 37);
            this.txtISBN_Upd.Name = "txtISBN_Upd";
            this.txtISBN_Upd.Size = new System.Drawing.Size(114, 20);
            this.txtISBN_Upd.TabIndex = 54;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(6, 18);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(73, 13);
            this.Label7.TabIndex = 52;
            this.Label7.Text = "Accession No";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(47, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(32, 13);
            this.Label8.TabIndex = 53;
            this.Label8.Text = "ISBN";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(24, 70);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(55, 13);
            this.Label9.TabIndex = 55;
            this.Label9.Text = "Book Title";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(41, 96);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(38, 13);
            this.Label11.TabIndex = 58;
            this.Label11.Text = "Author";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtKerko);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.dataGridView2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(586, 306);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "List of Books";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtKerko
            // 
            this.txtKerko.Location = new System.Drawing.Point(136, 76);
            this.txtKerko.Name = "txtKerko";
            this.txtKerko.Size = new System.Drawing.Size(100, 20);
            this.txtKerko.TabIndex = 2;
            this.txtKerko.TextChanged += new System.EventHandler(this.txtKerko_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "List of Books:";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(15, 112);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(547, 173);
            this.dataGridView2.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(28, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 25);
            this.label12.TabIndex = 37;
            this.label12.Text = "Manage Books";
            // 
            // ManageBooks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(691, 420);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tabAddBooks);
            this.MaximizeBox = false;
            this.Name = "ManageBooks";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManageBooks";
            this.Load += new System.EventHandler(this.ManageBooks_Load);
            this.tabAddBooks.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabAddBooks;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button btnClear;
        internal System.Windows.Forms.ComboBox CbCategory;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Button btnAddBooks;
        internal System.Windows.Forms.TextBox txtAccessionNo;
        internal System.Windows.Forms.TextBox txtAuthor;
        internal System.Windows.Forms.TextBox txtBookTitle;
        internal System.Windows.Forms.TextBox txtISBN;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView1;
        internal System.Windows.Forms.Button btnDeleteBook;
        internal System.Windows.Forms.Button btnUpdateDGV;
        internal System.Windows.Forms.ComboBox cbCategory_Upd;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtAccessionNo_Upd;
        internal System.Windows.Forms.TextBox txtAuthor_Upd;
        internal System.Windows.Forms.TextBox txtBookTitle_Upd;
        internal System.Windows.Forms.TextBox txtISBN_Upd;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtKerko;
    }
}