﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace LibraryManagement.Interface
{
    public interface IManage<TEntity> where TEntity : class
    {
        void Add(TEntity ojbType);
        void Update(TEntity ojbType);
        void Delete(TEntity ojbType);
        List<TEntity> GetAll();
        TEntity GetById(int id);
    }
}
