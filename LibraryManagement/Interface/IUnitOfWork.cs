﻿using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Interface
{
    public interface IUnitOfWork
    {
        IManage<Librat> BooksRepository { get; }
        IManage<Studentet> StudentsRepository { get; }
        IManage<LibratHuazuara> BorrowedBooksRepository { get; }
        IManage<LibriAutoret> BookAuthorsRepository { get; }
    }
}
