﻿using LibraryManagement.CustomModels;
using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Interface
{
    class ManageService<TEntity> : IManage<TEntity> where TEntity : class 
    {
        private readonly DbContext context;
        protected readonly DbSet<TEntity> dbSet;

        public ManageService(DbContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
        }

        public void Add(TEntity objType)
        {
            //shto ne databaze
            this.dbSet.Add(objType);
            //ruajme ne databaze
            this.context.SaveChanges();
        }

        public void Update(TEntity ojbType)
        {
            if (this.context.Entry(ojbType).State == EntityState.Detached)
            {
                this.dbSet.Attach(ojbType);
            }

            var entity = context.Entry(ojbType);
            this.context.Entry(ojbType).State = EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(TEntity ojbType)
        {
            this.context.Entry(ojbType).State = EntityState.Deleted;
            this.context.SaveChanges();
        }

        public List<TEntity> GetAll()
        {
            return this.dbSet.ToList();
        }

        public TEntity GetById(int id)
        {
            return this.dbSet.Find(id);
        }
    }
}
