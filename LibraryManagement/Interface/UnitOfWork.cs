﻿using LibraryManagement.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Interface
{
    public class UnitOfWork : IUnitOfWork
    {
        //nje context krijohjet per krejt 
        private readonly LibraryDBEntities context = new LibraryDBEntities();

        private IManage<Librat> booksRepository;
        private IManage<Studentet> studentsRepository;
        private IManage<LibratHuazuara> borrowedBooksRepository;
        private IManage<LibriAutoret> bookAuthorsRepository;

        public IManage<Librat> BooksRepository
        {
            get
            {
                return this.booksRepository ?? (this.booksRepository = new ManageService<Librat>(this.context));
            }
        }

        public IManage<Studentet> StudentsRepository
        {
            get
            {
                return this.studentsRepository ?? (this.studentsRepository = new ManageService<Studentet>(this.context));
            }
        }

        public IManage<LibratHuazuara> BorrowedBooksRepository
        {
            get
            {
                return this.borrowedBooksRepository ?? (this.borrowedBooksRepository = new ManageService<LibratHuazuara>(this.context));
            }
        }

        public IManage<LibriAutoret> BookAuthorsRepository
        {
            get
            {
                return this.bookAuthorsRepository ?? (this.bookAuthorsRepository = new ManageService<LibriAutoret>(this.context));
            }
        }

    }
}
