﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagement.Enums
{
    enum  Categories
    {
        Mathematics = 1,
        Computer = 2,
        Science = 3,
        Programming = 4,
        Novel = 5,
        Religion = 6
    }
}
