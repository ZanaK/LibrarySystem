﻿namespace LibraryManagement
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnBorrowBook = new System.Windows.Forms.Button();
            this.btnSearchBooks = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.btnManageStudents = new System.Windows.Forms.Button();
            this.btnManageBooks = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBorrowBook
            // 
            this.btnBorrowBook.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnBorrowBook.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBorrowBook.Location = new System.Drawing.Point(175, 124);
            this.btnBorrowBook.Name = "btnBorrowBook";
            this.btnBorrowBook.Size = new System.Drawing.Size(73, 44);
            this.btnBorrowBook.TabIndex = 29;
            this.btnBorrowBook.Text = "Borrow Book";
            this.btnBorrowBook.UseVisualStyleBackColor = false;
            this.btnBorrowBook.Click += new System.EventHandler(this.btnBorrowBook_Click);
            // 
            // btnSearchBooks
            // 
            this.btnSearchBooks.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearchBooks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearchBooks.Location = new System.Drawing.Point(178, 30);
            this.btnSearchBooks.Name = "btnSearchBooks";
            this.btnSearchBooks.Size = new System.Drawing.Size(73, 46);
            this.btnSearchBooks.TabIndex = 28;
            this.btnSearchBooks.Text = "Search Book";
            this.btnSearchBooks.UseVisualStyleBackColor = false;
            this.btnSearchBooks.Visible = false;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnLogOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogOut.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLogOut.Location = new System.Drawing.Point(178, 216);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(73, 44);
            this.btnLogOut.TabIndex = 27;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // btnManageStudents
            // 
            this.btnManageStudents.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnManageStudents.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnManageStudents.ForeColor = System.Drawing.Color.Black;
            this.btnManageStudents.Location = new System.Drawing.Point(51, 124);
            this.btnManageStudents.Name = "btnManageStudents";
            this.btnManageStudents.Size = new System.Drawing.Size(73, 45);
            this.btnManageStudents.TabIndex = 26;
            this.btnManageStudents.Text = "Manage Students";
            this.btnManageStudents.UseVisualStyleBackColor = false;
            this.btnManageStudents.Click += new System.EventHandler(this.btnManageStudents_Click);
            // 
            // btnManageBooks
            // 
            this.btnManageBooks.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnManageBooks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnManageBooks.Location = new System.Drawing.Point(299, 124);
            this.btnManageBooks.Name = "btnManageBooks";
            this.btnManageBooks.Size = new System.Drawing.Size(73, 44);
            this.btnManageBooks.TabIndex = 25;
            this.btnManageBooks.Text = "Manage Books";
            this.btnManageBooks.UseVisualStyleBackColor = false;
            this.btnManageBooks.Click += new System.EventHandler(this.btnManageBooks_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnSearchBooks;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CancelButton = this.btnLogOut;
            this.ClientSize = new System.Drawing.Size(430, 285);
            this.Controls.Add(this.btnBorrowBook);
            this.Controls.Add(this.btnSearchBooks);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnManageStudents);
            this.Controls.Add(this.btnManageBooks);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Library";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnBorrowBook;
        internal System.Windows.Forms.Button btnSearchBooks;
        internal System.Windows.Forms.Button btnLogOut;
        internal System.Windows.Forms.Button btnManageStudents;
        internal System.Windows.Forms.Button btnManageBooks;
    }
}

